{{/*
Expand the name of the chart.
*/}}
{{- define "ci-cd-workshop.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "ci-cd-workshop.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "ci-cd-workshop.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "ci-cd-workshop.labels" -}}
helm.sh/chart: {{ include "ci-cd-workshop.chart" . }}
{{ include "ci-cd-workshop.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- include "ci-cd-workshop.gitlabcilabels" . }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "ci-cd-workshop.selectorLabels" -}}
app.kubernetes.io/name: {{ include "ci-cd-workshop.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "ci-cd-workshop.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "ci-cd-workshop.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
GitLab CI labels
*/}}
{{- define "ci-cd-workshop.gitlabcilabels" -}}
{{- if .Values.ciVars }}
{{- if .Values.ciVars.CI_ENVIRONMENT_SLUG }}
app.gitlab.com/env: {{ tpl .Values.ciVars.CI_ENVIRONMENT_SLUG . | quote }}
{{- end }}
{{- if .Values.ciVars.CI_PROJECT_PATH_SLUG }}
app.gitlab.com/app: {{ tpl .Values.ciVars.CI_PROJECT_PATH_SLUG . | quote }}
{{- end }}
{{- end }}
{{- end }}